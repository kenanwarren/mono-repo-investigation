module gitlab.com/escribirio/escribir-io-monorepo/pubsub-service

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/go-chi/chi v3.3.3+incompatible
	golang.org/x/net v0.0.0-20181023162649-9b4f9f5ad519 // indirect
	golang.org/x/text v0.3.0 // indirect
)
