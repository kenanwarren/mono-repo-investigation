# Mono Repo Strategy Investigation on Gitlab
So I investigated gitlab this weekend as a possible service to migrate to because
bitbucket pipelines is leaving a lot to be desired. I've laid out a couple of
different build strategies for our mono repo below and some documentation
about extra goodies gitlab has below:

## Links
- https://docs.gitlab.com/ee/user/project/clusters/ 
- https://docs.gitlab.com/ee/topics/autodevops/index.html (Would need work to get it working with the mono repo)
- https://docs.gitlab.com/ee/ci/yaml So many more options!
- https://about.gitlab.com/devops-tools/ Click the little fox under each to read.

## Extra Info
- Runners take awhile to get picked up without connecting it to a kubernetes cluster.
- On top of making our runners quicker with the kubernetes integration we also get prometheus pretty tightly integrated with the devops metrics view. See https://docs.gitlab.com/ee/user/project/clusters/#installing-applications for other apps (Not much else but you can use helm.)

## Commit Message Strategy
- Idea from https://github.com/BastiPaeltz/gitlab-ci-monorepo
- See commit 3dc900455d3914920196b3b8fa8448fbe3042a3e for an example
- Boils down to adding a tag your commit to trigger the build.
### Pros
- More control over when to trigger pipelines
- Can get more granular by adding or subtracting commits that trigger special stages.
### Cons
- Manual in terms of remembering to prepend it to your commit
- Can't run a custom docker image for each project unless we run docker in docker (yo dog)

## Only Changes Strategy
- Found at https://docs.gitlab.com/ee/ci/yaml/#only-changes
- See commit 4263cebcc2e46dbe917cbd5f4b428497b6323574 for an example
- Builds are triggered based on changes to their respective directories.
### Pros
- Automatically triggers the build pipeline for that project in the mono repo.
- Low contextual overhead when making commits, make changes pipelines trigger.
### Cons
- Getting granular is more difficult with when to trigger certain stages
- Can't run a custom docker image for each project unless we run docker in docker (yo dog)

## Kitchen Sink (Awesome-Inc)
- Found at https://github.com/awesome-inc/monorepo.gitlab
- No commit in repo, just read through the docs.
### Pros
- Seems to cover a lot of different use cases.
- Can use different docker images (or at least it looks that way)
- Can customize to only build if changed.
### Cons
- Need to submodule a thirdparty repo or vendor it.
- Bit of black magic because I'm too lazy to read the source right now.

## Makefile + Docker Inception Hell
- Our old way in bitbucket pipelines
- See my merge request or the master repo as they're basically the same.
### Pros
- Can customize it to our liking
- Can use different docker images
### Cons
- Lot of black magic
- Need a lot of documentation to get all the devs to understand what's going on.
- Build process is slow (parallel builds help a little)
- Lots of black magic

