package main

import (
	"fmt"
	"net/http"

	"github.com/caarlos0/env"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type config struct {
	Port int `env:"PORT" envDefault:"3000"`
}

func main() {
	cfg := config{}
	err := env.Parse(&cfg)
	if err != nil {
		panic(err)
	}
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("fake payments api"))
	})
	http.ListenAndServe(fmt.Sprintf(":%d", cfg.Port), r)
}
